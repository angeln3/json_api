<?php
function getDepartamentos()
	{
		$data = array(
		$title = "Departamentos",
	    $content = array(
	        array(
	            "id" => 4496,
				"name" => "Muebles",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4497,
				"name" => "Hogar",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4511,
				"name" => "Línea blanca y Electrodomésticos",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4499,
				"name" => "Electrónica y Tecnología",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4550,
				"name" => "Telefonía",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 16649,
				"name" => "Videojuegos",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4500,
				"name" => "Pasatiempo y Diversión",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 5914,
				"name" => "Deportes",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4498,
				"name" => "Ferretería y Jardinería",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 5916,
				"name" => "Ella",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 5915,
				"name" => "Él",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4502,
				"name" => "Niños y Bebés",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 16100,
				"name" => "Zapatería",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4503,
				"name" => "Belleza",
				"action" => "category2Level"
	        )
		)
		);
	    $this->JSOND(true,"TODO OK",$data,200);
}
?>