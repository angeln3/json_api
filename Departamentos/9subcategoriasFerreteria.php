<?php

function getFerreteria_DP()
	{
	    $data =
	    array(
				"id" => 4498,
                "title" => "Ferretería y Jardinería",
                $content => 
                array(
					array(
                        "id"=> 5732,
                        "name"=> "Mecánica",
                        "img"=>""
				    ),
                    array(
                        "id"=> 5761,
                        "name"=> "Plomería",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5723,
                        "name"=> "Material Eléctrico",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5708,
                        "name"=> "Maquinaría",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5700,
                        "name"=> "Herramienta Eléctrica",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5697,
                        "name"=> "Compresoras",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5683,
                        "name"=> "Carpintería",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5675,
                        "name"=> "Cajas de Herramienta",
                        "img"=>""
                    ),
                    array(
                        "id"=> 11765,
                        "name"=> "Aspiradoras",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5668,
                        "name"=> "Accesorios Maquinaria",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5764,
                        "name"=> "Seguridad",
                        "img"=>""
                    ),
                    array(
                        "id"=> 16451,
                        "name"=> "Herramienta Industrial",
                        "img"=>""
                    ),
                    array(
                        "id"=> 5814,
                        "name"=> "Jardinería",
                        "img"=>""
                    )
				)
	    );
	    $this->JSOND(true,"TODO OK",$data,200);
	}