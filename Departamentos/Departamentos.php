<?php

function getDepartamentos()
{
    $data =
    array(
        array(
            "id"=> 4496,
            "name"=> "Muebles",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4497,
            "name"=> "Hogar",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4511,
            "name"=> "Línea blanca y Electrodomésticos",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4499,
            "name"=> "Electrónica y Tecnología",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4550,
            "name"=> "Telefonía",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 16649,
            "name"=> "Videojuegos",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4500,
            "name"=> "Pasatiempo y Diversión",
            "action"=> "category2Level"
        ),        
        array(
            "id"=> 5914,
            "name"=> "Deportes",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4498,
            "name"=> "Ferretería y Jardinería",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 5916,
            "name"=> "Ella",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 5915,
            "name"=> "Él",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4502,
            "name"=> "Niños y Bebés",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 16100,
            "name"=> "Zapatería",
            "action"=> "category2Level"
        ),
        array(
            "id"=> 4503,
            "name"=> "Belleza",
            "action"=> "category2Level"
        )
    );
    $this->JSOND(true,"TODO OK",$data,200);
}

function getSubcategorias()
{
    $data =
    array(
        array(
            "id"=> 4496,
            $data = 
            array(
                array(
                    "id"=> 10975,
                    "name"=> "Colchones y Boxes",
                    "img"=> "",
                    "action"=> "Category"
                ),
                array(
                    "id"=> 4523,
                    "name"=> "Salas",
                    "img"=> "",
                    "action"=> "Category"
                ),
                array(
                    "id"=> 6625,
                    "name"=> "Cuarto de TV",
                    "img"=> "",
                    "action"=> "Category"
                ),
                array(
                    "id"=> 4522,
                    "name"=> "Recámaras",
                    "img"=> "",
                    "action"=> "Category"
                )
            )
        ),//fin subcateroria 1
        array(
            "id"=> 4497,
            $data = 
            array(
                array(
                    "id"=> 4505,
                    "name"=> "Cocina",
                    "img"=> "",
                    "action"=> "Kitchen"
                ),
                array(
                    "id"=> 4506,
                    "name"=> "Cristalería y Vajillas",
                    "img"=> "",
                    "action"=> "Glassware"
                ),
                array(
                    "id"=> 4510,
                    "name"=> "Decoración",
                    "img"=> "",
                    "action"=> "Decoration"
                ),
                array(
                    "id"=> 4803,
                    "name"=> "Blancos",
                    "img"=> "",
                    "action"=> "White"
                )
            )
        ),//fin subcateroria 2
        array(
            "id"=> 4511,
            $data = 
            array(
                array(
                    "id"=> 10667,
                    "name"=> "Lavado",
                    "img"=> "",
                    "action"=> "Washed"
                ),
                array(
                    "id"=> 10524,
                    "name"=> "Cocción",
                    "img"=> "",
                    "action"=> "Cooking"
                ),
                array(
                    "id"=> 15523,
                    "name"=> "Refrigeración",
                    "img"=> "",
                    "action"=> "Refrigeration"
                ),
                array(
                    "id"=> 4508,
                    "name"=> "Electrodomésticos",
                    "img"=> "",
                    "action"=> "Home Appliances"
                )
            )
        ),//fin subcateroria 3
        array(
            "id"=> 4499,
            $data = 
            array(
                array(
                    "id"=> 4551,
                    "name"=> "TV y Vídeo",
                    "img"=> "",
                    "action"=> "TV and Video"
                ),
                array(
                    "id"=> 4548,
                    "name"=> "Computo",
                    "img"=> "",
                    "action"=> "Computers"
                ),
                array(
                    "id"=> 4549,
                    "name"=> "Fotografía",
                    "img"=> "",
                    "action"=> "Photography"
                ),
                array(
                    "id"=> 4547,
                    "name"=> "Audio",
                    "img"=> "",
                    "action"=> "Audio"
                )
            )
        ),//fin subcateroria 4
        array(
            "id"=> 4550,
            $data = 
            array(
                array(
                    "id"=> 16108,
                    "name"=> "Celulares",
                    "img"=> "",
                    "action"=> "Cell Phones"
                ),
                array(
                    "id"=> 10098,
                    "name"=> "Fija",
                    "img"=> "",
                    "action"=> "Fixed"
                ),
                array(
                    "id"=> 4610,
                    "name"=> "Accesorios",
                    "img"=> "",
                    "action"=> "Accesories"
                )
            )
        ),//fin subcateroria 5
        array(
            "id"=> 16649,
            $data = 
            array(
                array(
                    "id"=> 16659,
                    "name"=> "XBOX",
                    "img"=> "",
                    "action"=> "XBOX"
                ),
                array(
                    "id"=> 16667,
                    "name"=> "PlayStation",
                    "img"=> "",
                    "action"=> "PlayStation"
                ),
                array(
                    "id"=> 16663,
                    "name"=> "Nintendo",
                    "img"=> "",
                    "action"=> "Nintendo"
                ),
                array(
                    "id"=> 16673,
                    "name"=> "Preventas",
                    "img"=> "",
                    "action"=> "Preventas"
                )
            )
        ),//fin subcateroria 6
        array(
            "id"=> 4500,
            $data = 
            array(
                array(
                    "id"=> 4556,
                    "name"=> "Juguetería",
                    "img"=> "",
                    "action"=> "Toy Store"
                ),
                array(
                    "id"=> 4555,
                    "name"=> "Discos y Vídeo",
                    "img"=> "",
                    "action"=> "Discs and Video"
                ),
                array(
                    "id"=> 5022,
                    "name"=> "Instrumentos Musicales",
                    "img"=> "",
                    "action"=> "Musical Instruments"
                ),
                array(
                    "id"=> 4516,
                    "name"=> "Equipaje",
                    "img"=> "",
                    "action"=> "Luggage"
                )
            )
        ),//fin subcateroria 7
        array(
            "id"=> 5914,
            $data = 
            array(
                array(
                    "id"=> 4633,
                    "name"=> "Aparatos de Ejercicio",
                    "img"=> "",
                    "action"=> "Exercise Machines"
                ),
                array(
                    "id"=> 6440,
                    "name"=> "Calzado Deportivo",
                    "img"=> "",
                    "action"=> "Sports Shoes"
                ),
                array(
                    "id"=> 6545,
                    "name"=> "Ropa Deportiva",
                    "img"=> "",
                    "action"=> "Sportswear"
                ),
                array(
                    "id"=> 4554,
                    "name"=> "Accesorios Deportivos",
                    "img"=> "",
                    "action"=> "Sports Accesories"
                )
            )
        ),//fin subcateroria 8
        array(
            "id"=> 4498,
            $data = 
            array(
                array(
                    "id"=> 5732,
                    "name"=> "Mecánica",
                    "img"=> "",
                    "action"=> "Mechanics"
                ),
                array(
                    "id"=> 5761,
                    "name"=> "Plomería",
                    "img"=> "",
                    "action"=> "Plumbing"
                ),
                array(
                    "id"=> 5723,
                    "name"=> "Material Eléctico",
                    "img"=> "",
                    "action"=> "Electric Material"
                ),
                array(
                    "id"=> 5708,
                    "name"=> "Maquinaría",
                    "img"=> "",
                    "action"=> "Machinery"
                )
            )
        ),//fin subcateroria 9
        array(
            "id"=> 5916,
            $data = 
            array(
                array(
                    "id"=> 4902,
                    "name"=> "Moda Damas",
                    "img"=> "",
                    "action"=> "Ladies Fashion"
                ),
                array(
                    "id"=> 5222,
                    "name"=> "Juniors",
                    "img"=> "",
                    "action"=> "Juniors"
                ),
                array(
                    "id"=> 5229,
                    "name"=> "Petite",
                    "img"=> "",
                    "action"=> "Petite"
                ),
                array(
                    "id"=> 12198,
                    "name"=> "Maternidad",
                    "img"=> "",
                    "action"=> "Maternity"
                )
            )
        ),//fin subcateroria 10
        array(
            "id"=> 5915,
            $data = 
            array(
                array(
                    "id"=> 16211,
                    "name"=> "Moda Casual",
                    "img"=> "",
                    "action"=> "Casual Fashion"
                ),
                array(
                    "id"=> 16210,
                    "name"=> "Moda Formal",
                    "img"=> "",
                    "action"=> "Formal Fashion"
                ),
                array(
                    "id"=> 6806,
                    "name"=> "Universitarios",
                    "img"=> "",
                    "action"=> "University Students"
                ),
                array(
                    "id"=> 16246,
                    "name"=> "Talla Plus Casual",
                    "img"=> "",
                    "action"=> "Plus Size Casual"
                )
            )
        ),//fin subcateroria 11
        array(
            "id"=> 4502,
            $data = 
            array(
                array(
                    "id"=> 4571,
                    "name"=> "Muebles y Accesorios para Bebés",
                    "img"=> "",
                    "action"=> "Baby Ferniture and Accessories"
                ),
                array(
                    "id"=> 15369,
                    "name"=> "Ropa para Bebés",
                    "img"=> "",
                    "action"=> "Baby Clothes"
                ),
                array(
                    "id"=> 4573,
                    "name"=> "Ropa para Niñas ",
                    "img"=> "",
                    "action"=> "Clothes for Girls"
                ),
                array(
                    "id"=> 4572,
                    "name"=> "Ropa para Niños",
                    "img"=> "",
                    "action"=> "Clotes for Children"
                )
            )
        ),//fin subcateroria 12
        array(
            "id"=> 16100,
            $data = 
            array(
                array(
                    "id"=> 5249,
                    "name"=> "Ella",
                    "img"=> "",
                    "action"=> "She"
                ),
                array(
                    "id"=> 4908,
                    "name"=> "Él",
                    "img"=> "",
                    "action"=> "He"
                ),
                array(
                    "id"=> 5084,
                    "name"=> "Niñas",
                    "img"=> "",
                    "action"=> "Girls"
                ),
                array(
                    "id"=> 5088,
                    "name"=> "Niños",
                    "img"=> "",
                    "action"=> "Children"
                )
            )
        ),//fin subcateroria 13
        array(
            "id"=> 4503,
            $data = 
            array(
                array(
                    "id"=> 15685,
                    "name"=> "Fragancias",
                    "img"=> "",
                    "action"=> "Fragances"
                ),
                array(
                    "id"=> 15693,
                    "name"=> "Cosméticos",
                    "img"=> "",
                    "action"=> "Cosmetics"
                ),
                array(
                    "id"=> 15701,
                    "name"=> "Tratamientos",
                    "img"=> "",
                    "action"=> "Treatments"
                ),
                array(
                    "id"=> 10824,
                    "name"=> "Vida y Salud",
                    "img"=> "",
                    "action"=> "Life and Health"
                )
            )
        ),//fin subcateroria 14
    );
    $this->JSOND(true,"TODO OK",$data,200);
}
?>