<?php

function getProduccto_salas_SDP()
{
    $data =
    array(
        array(
            "id"=> 184128,
            "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2966420.jpg",
            "title"=> "Chaise Pisa en Velvet Cream Home Nature",
            "price"=> 6999,
            "sale_price"=> 6999,
            "discount"=> 0,
            "categories"=> array(
                array(
                    "idCategoria"=> 4496,
                    "nombre"=> "Muebles",
                    "idPadre"=> 4496,
                    "categoriasHijas"=> array(
                        array(
                            "id"=> 4496,
                            "name"=> "Muebles",
                            "seo"=> "muebles",
                            "path_length"=> 0
                        ),
                        array(
                            "id"=> 4523,
                            "name"=> "Salas",
                            "seo"=> "salas",
                            "path_length"=> 1
                        ),
                        array(
                            "id"=> 4777,
                            "name"=> "Salas por piezas",
                            "seo"=> "salas-por-piezas",
                            "path_length"=> 2,
                            "subcategory"=> array(
                                array(                                    
                                    "id"=>"10652",
                                    "name"=> "Chaise long",
                                    "seo"=> "chaise-long",
                                    "path_length"=> 0
                                    )
                                    )   
                                    )
                                    )
                                    )                                    
                                    )
                                    )
                                    
                                );
	    $this->JSOND(true,"TODO OK",$data,200);
	}