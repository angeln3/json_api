<?php
function getSubCategoriaProducto()
{
    $data =
    array(
        array(
            "id"=> 10975,
			"name"=> "Colchones y Boxes",
            "productos"=> array(
                array(
                    "id"=> 104651,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2723950.jpg",
                    "title" => "Juego De Colchón y Box Matrimonial Duo Bed Plus Restonic",
                    "price" => 11399,
                    "price_sale" => 28499,
                    "discount" => 60
                ),
                array(
                    "id"=> 133132,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2810486.jpg",
                    "title" => "Juego De Colchón Y Box Individual Duo Bed Plus Restonic",
                    "price" => 9199,
                    "price_sale" => 22999,
                    "discount" => 60
                ),
                array(
                    "id"=> 172273,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2924761.jpg",
                    "title" => "Juego De Colchón Y Box Matrimonial Frida Spring Air",
                    "price" => 7499,
                    "price_sale" => 18999,
                    "discount" => 61
                ),
                array(
                    "id"=> 103907,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2802155.jpg",
                    "title" => "Juego De Colchón Y Box Queen Size Ametist Serta",
                    "price" => 7739,
                    "price_sale" => 21499,
                    "discount" => 64
                ),
                array(
                    "id"=> 102121,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2672058.jpg",
                    "title" => "Colchón Individual Mónaco Plus Restonic",
                    "price" => 3399,
                    "price_sale" => 8499,
                    "discount" => 60
                ),
                array(
                    "id"=> 104572,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2802156.jpg",
                    "title" => "Juego De Colchón Y Box King Size Ametist Serta",
                    "price" => 11049,
                    "price_sale" => 30699,
                    "discount" => 64
                ),
                array(
                    "id"=> 103727,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2802154.jpg",
                    "title" => "Juego De Colchón Y Box Matrimonial Ametist Serta",
                    "price" => 7199,
                    "price_sale" => 19999,
                    "discount" => 64
                ),
                array(
                    "id"=> 103507,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2680273.jpg",
                    "title" => "Colchón Matrimonial Summit Plus Restonic",
                    "price" => 5999,
                    "price_sale" => 14999,
                    "discount" => 60
                ),
                array(
                    "id"=> 103599,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2672064.jpg",
                    "title" => "Colchón King Size Mónaco Plus Restonic",
                    "price" => 6319,
                    "price_sale" => 15799,
                    "discount" => 60
                ),
                array(
                    "id"=> 102883,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2672060.jpg",
                    "title" => "Colchón Restonic Matrimonial Mónaco Plus",
                    "price" => 4399,
                    "price_sale" => 10999,
                    "discount" => 60
                ),
                array(
                    "id"=> 160512,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2888490.jpg",
                    "title" => "Juego Queen Size Versant Serta",
                    "price" => 17639,
                    "price_sale" => 48999,
                    "discount" => 64
                ),
                array(
                    "id"=> 103094,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2672062.jpg",
                    "title" => "Colchón Queen Size Mónaco Plus Restonic",
                    "price" => 4799,
                    "price_sale" => 11999,
                    "discount" => 60
                ),
                array(
                    "id"=> 155330,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2874549.jpg",
                    "title" => "Colchón Matrimonial 50 Aniversario Vittorio Benzi",
                    "price" => 4599,
                    "price_sale" => 8999,
                    "discount" => 49
                ),
                array(
                    "id"=> 140064,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2824249.jpg",
                    "title" => "Juego Matrimonial Scandinavian Narvik Spring Air",
                    "price" => 11199,
                    "price_sale" => 27999,
                    "discount" => 60
                ),
                array(
                    "id"=> 105399,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2594256.jpg",
                    "title" => "Juego De Colchón Y Box Queen Size Montecarlo Sealy",
                    "price" => 24399,
                    "price_sale" => 60999,
                    "discount" => 60
                ),
                array(
                    "id"=> 105566,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2594257.jpg",
                    "title" => "Juego De Colchón Y Box King Size Montecarlo Sealy",
                    "price" => 32399,
                    "price_sale" => 80999,
                    "discount" => 60
                ),
                array(
                    "id"=> 104152,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2680275.jpg",
                    "title" => "Colchón King Size Summit Plus Restonic",
                    "price" => 8519,
                    "price_sale" => 21299,
                    "discount" => 60
                ),
                array(
                    "id"=> 155332,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2874635.jpg",
                    "title" => "Juego Matrimonial Maple Restonic",
                    "price" => 6699,
                    "price_sale" => 10999,
                    "discount" => 60
                ),
                array(
                    "id"=> 160509,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2888494.jpg",
                    "title" => "Juego Queen Size Heavenly Serta",
                    "price" => 16399,
                    "price_sale" => 40999,
                    "discount" => 39
                ),
                array(
                    "id"=> 103787,
                    "img" => "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/340x260/2757221.jpg",
                    "title" => "Colchón Matrimonial Bamboo Plus Restonic",
                    "price" => 6919,
                    "price_sale" => 17299,
                    "discount" => 60
                )
            )
        )
    );
    $this->JSOND(true,"TODO OK",$data,200);
}
?>