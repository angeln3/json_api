<?php
//cabeceras para que solo acepte datos json
header("Access-Control-Allow-Origin: http://clauluna.com/CLARO/api/login");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
function post_login( $emailFijo = "correo@mail.com", $passwordFijo = "123456" )
{
    $email = $_POST["email"]; //el name del campo input del correo
    $password = $_POST["password"]; //el name del campo input de la contraseña

    if( $email === $emailFijo && $password === $passwordFijo )
    {
        $token = "1122334455"; 
        $nombre = "Pedro";
        session_start();
        $_SESSION["email"] = $email;
        //header("Location: home.html"); //redireccionar a la pagina luego de iniciar sesión (home)
        
        ini_set("display_errors",0);
        header("Content-Type: application/json; charset=UTF-8");
        $this->JSONDLoginTrue(true,"Inicio Exitoso",$token,200);
    }
    else
    {
        $this->JSONDLogin(true,"Usuario o Contraseña incorrectos, por favor intente de nuevo.",401);
    }

}

function recover_password( $emailFijo = "correo@mail.com" )
{
    $email = $_POST["email"]; //el name del campo input del correo

    if( $emailFijo === $email)
    {
        $this->JSONDLogin(true,"Se ha enviado a la información a su correo.",200);
    }
    else
    {
        $this->JSONDLogin(true,"El correo suministrado no existe",401);
    }
}

function register_User()
{
    $name = $_POST["name"]; //el name del campo input nombre
    $last_name = $_POST["last_name"]; //el name del campo input apellido paterno
    $email = $_POST["email"]; //clave valor del campo input correo
    $password = $_POST["password"]; //clave valor del campo input password
    $confirm_password = $_POST["confirm_password"]; //clave valor del campo input confirmar password
    $correoRegistrado = "correo@mail.com";

   if( $name != null && $last_name != null && $email && $password !=null && $confirm_password !=null)
   {
       if(strcmp ($password , $confirm_password ) == 0)
       {
           if($email != $correoRegistrado)
           {
            $this->JSONDLogin(true,"Se ha registrado de forma exitosa.",200);
           }
           else{
            $this->JSONDLogin(true,"El correo ya se encuentra registrado.",200);
           }
         
       }
       else
       {
         $this->JSONDLogin(true,"Las contraseñas no coinciden",401);
       }
   }
   else
   {
        $this->JSONDLogin(true,"Los campos son obligatorios.",401);
   }
}

function JSONDLoginTrue($status,$msg,$data,$code)
{
    $response = array("success" => $status, "msg" => $msg , "token" => $token,"code" => $code);
    echo json_encode($response);
}

function JSONDLogin($status,$msg,$data,$code)
{
    $response = array("success" => $status, "msg" => $msg , "code" => $code);
    echo json_encode($response);
}
?>