<?php

function getOfertas()
{
    $data =
    array(
        array(
            "id"=> 139618,
            "title"=> "Celular Nokia 5.1 1081 Color Cobre R9 (Telcel)",
            "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2820213.jpg",
            "price"=> 3349,
            "price_sale"=> 5499,
            "discount"=> 39,
            "categories"=> array(
                array(
                    "idCategoria"=> 4550,
                    "nombre"=> "Telefonia",
                    "idPadre"=> 4550,
                    "categoriasHijas"=> array(
                        array(
                            "id"=> 4550,
                            "name"=> "Telefonía",
                            "seo"=> "telefonia",
                            "path_length" => 0
                        ),
                        array(
                            "id"=> 16108,
                            "name"=> "Celulares",
                            "seo"=> "celulares",
                            "path_length" => 1
                        ),
                        array(
                            "id"=> 15092,
                            "name"=> "Nokia",
                            "seo"=> "nokia",
                            "path_length" => 2
                        )
                            )
                            )
                            
                            )
                        ),
        array(
            "id"=> 156840,
            "title"=> "Celular Tp- Link C7A Color Grey Open",
            "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2879233.jpg",
            "price"=> 1519,
            "price_sale"=> 1899,
            "discount"=> 20,
            "categories"=> array(
                array(
                    "idCategoria"=> 4550,
                    "nombre"=> "Telefonía",
                    "idPadre"=> 4550,
                    "categoriasHijas"=> array(
                        array(
                            "id"=> 4550,
                            "name"=> "Telefonía",
                            "seo"=> "telefonia",
                            "path_length" => 0
                        ),
                        array(
                            "id"=> 16108,
                            "name"=> "Celulares",
                            "seo"=> "celulares",
                            "path_length" => 1
                        ),
                        array(
                            "id"=> 16644,
                            "name"=> "Neffos",
                            "seo"=> "neffos",
                            "path_length" => 2
                            )
                            )
                            )
                            
                            )
                        ),
        array(
            "id"=> 156847,
            "title"=> "Celular Tp- Link C9 Color Moonlight Silvery Open",
            "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2879309.jpg",
            "price"=> 2249,
            "price_sale"=> 2999,
            "discount"=> 25,
            "categories"=> array(
                array(
                    "idCategoria"=> 4550,
                    "nombre"=> "Telefonía",
                    "idPadre"=> 4550,
                    "categoriasHijas"=> array(
                        array(
                            "id"=> 4550,
                            "name"=> "Telefonía",
                            "seo"=> "telefonia",
                            "path_length" => 0
                        ),
                        array(
                            "id"=> 16108,
                            "name"=> "Celulares",
                            "seo"=> "celulares",
                            "path_length" => 1
                        ),
                        array(
                            "id"=> 16644,
                            "name"=> "Neffos",
                            "seo"=> "neffos",
                            "path_length" => 2
                        )
                        
                            )
                            )
                            
                            )
                        ),
            array(
                "id"=> 105561,
                "title"=> "Pantalla Led Hisense 65'' 4K Uhd Smart 65H9E",
                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2782253.jpg",
                "price"=> 21109,
                "price_sale"=> 21109,
                "discount"=> 0,
                "categories"=> array(
                    array(
                        "idCategoria"=> 4499,
                        "nombre"=> "Electrónica y Tecnología",
                        "idPadre"=> 4499,
                        "categoriasHijas"=> array(
                            array(
                                "id"=> 4499,
                                "name"=> "Electrónica y Tecnología",
                                "seo"=> "electronica-y-tecnologia",
                                "path_length" => 0
                            ),
                            array(
                                "id"=> 4551,
                                "name"=> "TV y Vídep",
                                "seo"=> "tv-y-video",
                                "path_length" => 1
                            ),
                            array(
                                "id"=> 15466,
                                "name"=> "Pantallas",
                                "seo"=> "pantallas",
                                "path_length" => 2
                            ),
                            
                                )
                                )
                                
                                )
                            ),
                            array(
                                "id"=> 143972,
                                "title"=> "Audífonos Bluetooth Earbud Sapphiere Negro Happy Plugs",
                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2834746.jpg",
                                "price"=> 669,
                                "price_sale"=> 739,
                                "discount"=> 9 ,
                                "categories"=> array(
                                    array(
                                        "idCategoria"=> 4499,
                                        "nombre"=> "Electrónica y Tecnología",
                                        "idPadre"=> 4499,
                                        "categoriasHijas"=> array(
                                            array(
                                                "id"=> 4499,
                                                "name"=> "Electrónica y Tecnología",
                                                "seo"=> "electronica-y-tecnologia",
                                                "path_length" => 0
                                            ),
                                            array(
                                                "id"=> 4547,
                                                "name"=> "Audio",
                                                "seo"=> "audio",
                                                "path_length" => 1
                                            ),
                                            array(
                                                "id"=> 15106,
                                                "name"=> "Audífonos",
                                                "seo"=> "audifonos",
                                                "path_length" => 2
                                            ),
                                            
                                                )
                                                )
                                                
                                                )
                                            ),
                                            array(
                                                "id"=> 143608,
                                                "title"=> "Audífonos Earbud Deluxe Pink Gold Happy Plugs",
                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2834662.jpg",
                                                "price"=> 339,
                                                "price_sale"=> 369,
                                                "discount"=> 8 ,
                                                "categories"=> array(
                                                    array(
                                                        "idCategoria"=> 4499,
                                                        "nombre"=> "Electrónica y Tecnología",
                                                        "idPadre"=> 4499,
                                                        "categoriasHijas"=> array(
                                                            array(
                                                                "id"=> 4499,
                                                                "name"=> "Electrónica y Tecnología",
                                                                "seo"=> "electronica-y-tecnologia",
                                                                "path_length" => 0
                                                            ),
                                                            array(
                                                                "id"=> 4547,
                                                                "name"=> "Audio",
                                                                "seo"=> "audio",
                                                                "path_length" => 1
                                                            ),
                                                            array(
                                                                "id"=> 15106,
                                                                "name"=> "Audífonos",
                                                                "seo"=> "audifonos",
                                                                "path_length" => 2
                                                            )
                                                            
                                                                )
                                                                )
                                                                
                                                                )
                                                            ),
                                                            array(
                                                                "id"=> 143724,
                                                                "title"=> "Audífonos C/micrófono Earbud Plus Turquesa Happy Plugs",
                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2834683.jpg",
                                                                "price"=> 409,
                                                                "price_sale"=> 449,
                                                                "discount"=> 9 ,
                                                                "categories"=> array(
                                                                    array(
                                                                        "idCategoria"=> 4499,
                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                        "idPadre"=> 4499,
                                                                        "categoriasHijas"=> array(
                                                                            array(
                                                                                "id"=> 4499,
                                                                                "name"=> "Electrónica y Tecnología",
                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                "path_length" => 0
                                                                            ),
                                                                            array(
                                                                                "id"=> 4547,
                                                                                "name"=> "Audio",
                                                                                "seo"=> "audio",
                                                                                "path_length" => 1
                                                                            ),
                                                                            array(
                                                                                "id"=> 15106,
                                                                                "name"=> "Audífonos",
                                                                                "seo"=> "audifonos",
                                                                                "path_length" => 2
                                                                            )
                                                                            
                                                                            
                                                                                )
                                                                                )
                                                                                
                                                                                )
                                                                            ),
                                                                            array(
                                                                                "id"=> 143726,
                                                                                "title"=> "Audífonos C/micrófono Earbud Plus Negro Happy Plugs",
                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2834689.jpg",
                                                                                "price"=> 409,
                                                                                "price_sale"=> 449,
                                                                                "discount"=> 9 ,
                                                                                "categories"=> array(
                                                                                    array(
                                                                                        "idCategoria"=> 4499,
                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                        "idPadre"=> 4499,
                                                                                        "categoriasHijas"=> array(
                                                                                            array(
                                                                                                "id"=> 4499,
                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                "path_length" => 2
                                                                                            ),
                                                                                            array(
                                                                                                "id"=> 4547,
                                                                                                "name"=> "Audio",
                                                                                                "seo"=> "audio",
                                                                                                "path_length" => 1
                                                                                            ),    
                                                                                            array(
                                                                                                "id"=> 15106,
                                                                                                "name"=> "Audífonos",
                                                                                                "seo"=> "audifonos",
                                                                                                "path_length" => 0
                                                                                            )
                                                                                            
                                                                                            
                                                                                                )
                                                                                                )
                                                                                                
                                                                                                )
                                                                                            ),
                                                                                            array(
                                                                                                "id"=> 143727,
                                                                                                "title"=> "Audífonos C/micrófono Earbud Deluxe Oro Happy Plugs",
                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2834692.jpg",
                                                                                                "price"=> 409,
                                                                                                "price_sale"=> 449,
                                                                                                "discount"=> 9 ,
                                                                                                "categories"=> array(
                                                                                                    array(
                                                                                                        "idCategoria"=> 4499,
                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                        "idPadre"=> 4499,
                                                                                                        "categoriasHijas"=> array(
                                                                                                            array(
                                                                                                                "id"=> 4499,
                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                "path_length" => 0
                                                                                                            ),
                                                                                                            array(
                                                                                                                "id"=> 4547,
                                                                                                                "name"=> "Audio",
                                                                                                                "seo"=> "audio",
                                                                                                                "path_length" => 1
                                                                                                            ),
                                                                                                            array(
                                                                                                                "id"=> 15106,
                                                                                                                "name"=> "Audífonos",
                                                                                                                "seo"=> "audifonos",
                                                                                                                "path_length" => 2
                                                                                                            )
                                                                                                            
                                                                                                            
                                                                                                                )
                                                                                                                )
                                                                                                                
                                                                                                                )
                                                                                                            ),
                                                                                                            array(
                                                                                                                "id"=> 104440,
                                                                                                                "title"=> "Cámara Rebelt6 Canon Ef-S 18-55Mm",
                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2510317.jpg",
                                                                                                                "price"=> 8839,
                                                                                                                "price_sale"=> 12999,
                                                                                                                "discount"=> 32,
                                                                                                                "categories"=> array(
                                                                                                                    array(
                                                                                                                        "idCategoria"=> 4499,
                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                        "idPadre"=> 4499,
                                                                                                                        "categoriasHijas"=> array(
                                                                                                                            array(
                                                                                                                                "id"=> 4499,
                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                "path_length" => 0
                                                                                                                            ),
                                                                                                                            array(
                                                                                                                                "id"=> 4549,
                                                                                                                                "name"=> "Fotografía",
                                                                                                                                "seo"=> "fotografia",
                                                                                                                                "path_length" => 1
                                                                                                                            ),
                                                                                                                            array(
                                                                                                                                "id"=> 4609,
                                                                                                                                "name"=> "Réflex / Semiprofesionales",
                                                                                                                                "seo"=> "reflex-semiprofesionales",
                                                                                                                                "path_length" => 2
                                                                                                                            )
                                                                                                                            
                                                                                                                            
                                                                                                                                )
                                                                                                                                )
                                                                                                                                
                                                                                                                                )
                                                                                                                            ),
                                                                                                                            array(
                                                                                                                                "id"=> 105197,
                                                                                                                                "title"=> "Cámara Canon Eos Rebel Sl2 Lte Ef-S 18-55Mm Is Stm",
                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2733469.jpg",
                                                                                                                                "price"=> 14959,
                                                                                                                                "price_sale"=> 21999,
                                                                                                                                "discount"=>  32,
                                                                                                                                "categories"=> array(
                                                                                                                                    array(
                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                            array(
                                                                                                                                                "id"=> 4499,
                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                "path_length" => 0
                                                                                                                                            ),
                                                                                                                                            array(
                                                                                                                                                "id"=> 4549,
                                                                                                                                                "name"=> "Fotografía",
                                                                                                                                                "seo"=> "fotografia",
                                                                                                                                                "path_length" => 1
                                                                                                                                            ),
                                                                                                                                            array(
                                                                                                                                                "id"=> 4609,
                                                                                                                                                "name"=> "Réflex / Semiprofesionales",
                                                                                                                                                "seo"=> "reflex-semiprofesionales",
                                                                                                                                                "path_length" => 2
                                                                                                                                            )
                                                                                                                                            
                                                                                                                                                )
                                                                                                                                                )
                                                                                                                                                
                                                                                                                                                )
                                                                                                                                            ),
                                                                                                                                            array(
                                                                                                                                                "id"=> 138267,
                                                                                                                                                "title"=> "Kit Tripié Profesional 67 3 Vías Y Mono Pod Vivitar",
                                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2813876.jpg",
                                                                                                                                                "price"=> 1099,
                                                                                                                                                "price_sale"=> 1829,
                                                                                                                                                "discount"=>  40,
                                                                                                                                                "categories"=> array(
                                                                                                                                                    array(
                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                            array(
                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                "path_length" => 0
                                                                                                                                                            ),
                                                                                                                                                            array(
                                                                                                                                                                "id"=> 4549,
                                                                                                                                                                "name"=> "Fotografía",
                                                                                                                                                                "seo"=> "fotografia",
                                                                                                                                                                "path_length" => 1
                                                                                                                                                            ),
                                                                                                                                                            array(
                                                                                                                                                                "id"=> 4606,
                                                                                                                                                                "name"=> "Accesorios",
                                                                                                                                                                "seo"=> "Accesorios",
                                                                                                                                                                "path_length" => 2
                                                                                                                                                            )
                                                                                                                                                            
                                                                                                                                                                )
                                                                                                                                                                )
                                                                                                                                                                
                                                                                                                                                                )
                                                                                                                                                            ),
                                                                                                                                                            array(
                                                                                                                                                                "id"=> 105388,
                                                                                                                                                                "title"=> "Kit Cámara Nikon D5600Lk+ Estuche+Tripié+ Sd16Gb",
                                                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2679060.jpg",
                                                                                                                                                                "price"=> 18359,
                                                                                                                                                                "price_sale"=> 26999,
                                                                                                                                                                "discount"=>  32,
                                                                                                                                                                "categories"=> array(
                                                                                                                                                                    array(
                                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                                            array(
                                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                                "path_length" => 0
                                                                                                                                                                            ),
                                                                                                                                                                            array(
                                                                                                                                                                                "id"=> 4549,
                                                                                                                                                                                "name"=> "Fotografía",
                                                                                                                                                                                "seo"=> "fotografia",
                                                                                                                                                                                "path_length" => 1
                                                                                                                                                                            ),
                                                                                                                                                                            array(
                                                                                                                                                                                "id"=> 4609,
                                                                                                                                                                                "name"=> "Réflex / Semiprofesionales",
                                                                                                                                                                                "seo"=> "reflex-semiprofesionales",
                                                                                                                                                                                "path_length" => 2
                                                                                                                                                                            )
                                                                                                                                                                            
                                                                                                                                                                            
                                                                                                                                                                                )
                                                                                                                                                                                )
                                                                                                                                                                                
                                                                                                                                                                                )
                                                                                                                                                                            ),
                                                                                                                                                                            array(
                                                                                                                                                                                "id"=> 104713,
                                                                                                                                                                                "title"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2598321.jpg",
                                                                                                                                                                                "img"=> "Cámara Digital Nikon 16Mp 83X Lcd 3 Fhd P900 B",
                                                                                                                                                                                "price"=> 12375,
                                                                                                                                                                                "price_sale"=> 18199,
                                                                                                                                                                                "discount"=>  32,
                                                                                                                                                                                "categories"=> array(
                                                                                                                                                                                    array(
                                                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                                                            array(
                                                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                                                "path_length" => 0
                                                                                                                                                                                            ),
                                                                                                                                                                                            array(
                                                                                                                                                                                                "id"=> 4549,
                                                                                                                                                                                                "name"=> "Fotografía",
                                                                                                                                                                                                "seo"=> "fotografia",
                                                                                                                                                                                                "path_length" => 1
                                                                                                                                                                                            ),
                                                                                                                                                                                            array(
                                                                                                                                                                                                "id"=> 4707,
                                                                                                                                                                                                "name"=> "Cámaras",
                                                                                                                                                                                                "seo"=> "camaras",
                                                                                                                                                                                                "path_length" => 2
                                                                                                                                                                                            )
                                                                                                                                                                                            
                                                                                                                                                                                            
                                                                                                                                                                                                )
                                                                                                                                                                                                )
                                                                                                                                                                                                
                                                                                                                                                                                                )
                                                                                                                                                                                            ),
                                                                                                                                                                                            array(
                                                                                                                                                                                                "id"=> 104871,
                                                                                                                                                                                                "title"=> "Laptop Lenovo Ideapad 320-15Isk Ci5",
                                                                                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2667933.jpg",
                                                                                                                                                                                                "price"=> 14999,
                                                                                                                                                                                                "price_sale"=> 14999,
                                                                                                                                                                                                "discount"=>  0,
                                                                                                                                                                                                "categories"=> array(
                                                                                                                                                                                                    array(
                                                                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                                                                            array(
                                                                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                                                                "path_length" => 0
                                                                                                                                                                                                            ),
                                                                                                                                                                                                            array(
                                                                                                                                                                                                                "id"=> 4548,
                                                                                                                                                                                                                "name"=> "Cómputo",
                                                                                                                                                                                                                "seo"=> "computo",
                                                                                                                                                                                                                "path_length" => 1
                                                                                                                                                                                                            ),
                                                                                                                                                                                                            array(
                                                                                                                                                                                                                "id"=> 4603,
                                                                                                                                                                                                                "name"=> "Laptops",
                                                                                                                                                                                                                "seo"=> "laptops",
                                                                                                                                                                                                                "path_length" => 2
                                                                                                                                                                                                            )
                                                                                                                                                                                                            
                                                                                                                                                                                                            
                                                                                                                                                                                                                )
                                                                                                                                                                                                                )
                                                                                                                                                                                                                
                                                                                                                                                                                                                )
                                                                                                                                                                                                            ),
                                                                                                                                                                                                            array(
                                                                                                                                                                                                                "id"=> 126207,
                                                                                                                                                                                                                "title"=> "Laptop A515-51-31Zz Acer",
                                                                                                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2790300.jpg",
                                                                                                                                                                                                                "price"=> 9899,
                                                                                                                                                                                                                "price_sale"=> 12999,
                                                                                                                                                                                                                "discount"=>  24,
                                                                                                                                                                                                                "categories"=> array(
                                                                                                                                                                                                                    array(
                                                                                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                                                                                "path_length" => 0
                                                                                                                                                                                                                            ),
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4548,
                                                                                                                                                                                                                                "name"=> "Cómputo",
                                                                                                                                                                                                                                "seo"=> "computo",
                                                                                                                                                                                                                                "path_length" => 1
                                                                                                                                                                                                                            ),
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4603,
                                                                                                                                                                                                                                "name"=> "Laptops",
                                                                                                                                                                                                                                "seo"=> "laptops",
                                                                                                                                                                                                                                "path_length" => 2
                                                                                                                                                                                                                            )
                                                                                                                                                                                                                            
                                                                                                                                                                                                                            
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                                
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                            ),
                                                                                                                                                                                                                array(
                                                                                                                                                                                                                "id"=> 126207,
                                                                                                                                                                                                                "title"=> "Laptop A515-51-31Zz Acer",
                                                                                                                                                                                                                "img"=> "https://resources.sears.com.mx/medios-plazavip/fotos/productos_sears1/original/2790300.jpg",
                                                                                                                                                                                                                "price"=> 9899,
                                                                                                                                                                                                                "price_sale"=> 12999,
                                                                                                                                                                                                                "discount"=>  24,
                                                                                                                                                                                                                "categories"=> array(
                                                                                                                                                                                                                    array(
                                                                                                                                                                                                                        "idCategoria"=> 4499,
                                                                                                                                                                                                                        "nombre"=> "Electrónica y Tecnología",
                                                                                                                                                                                                                        "idPadre"=> 4499,
                                                                                                                                                                                                                        "categoriasHijas"=> array(
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4499,
                                                                                                                                                                                                                                "name"=> "Electrónica y Tecnología",
                                                                                                                                                                                                                                "seo"=> "electronica-y-tecnologia",
                                                                                                                                                                                                                                "path_length" => 0
                                                                                                                                                                                                                            ),
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4548,
                                                                                                                                                                                                                                "name"=> "Cómputo",
                                                                                                                                                                                                                                "seo"=> "computo",
                                                                                                                                                                                                                                "path_length" => 1
                                                                                                                                                                                                                            ),
                                                                                                                                                                                                                            array(
                                                                                                                                                                                                                                "id"=> 4603,
                                                                                                                                                                                                                                "name"=> "Laptops",
                                                                                                                                                                                                                                "seo"=> "laptops",
                                                                                                                                                                                                                                "path_length" => 2
                                                                                                                                                                                                                            )
                                                                                                                                                                                                                            
                                                                                                                                                                                                                            
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                                
                                                                                                                                                                                                                                )
                                                                                                                                                                                                                            )
                                                                                                                                                                        
                                                                                                                                                                        
                                                                                );
                                                                                $this->JSOND(true,"TODO OK",$data,200);
                                                                            }