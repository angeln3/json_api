<?php

function getServicios()
	{
		$data = array(
        "title"=> "Servicios Sears",
	    "content" => array(
	        array(
	            "name" => "Autocenter",
                "id" => 1,
                "template" => array(
                    array(
                        "img"=>"https://resources.sears.com.mx/medios-plazavip/publicidad/20190514111139cambio-de-aceite-b1jpg.jpg"
                    ),
                    array(
                        "img"=>"https://resources.sears.com.mx/medios-plazavip/swift/v1/Sears/landings/img/autoCenter_2.jpg"
                    ),
                    array(
                        "img"=>"https://resources.sears.com.mx/medios-plazavip/publicidad/20191001092036verificacion-octubre-2019jpg.jpg"
                    ),
                    array(
                        "tabs"=> array(
                            array(
                                "id"=> 2,
                                "title"=> "Servicios Autocenter",
                                "content" => array(
                                    array(
                                        "type" => "Preventivos",
                                        "child" => array(
                                            array(
                                                "img"=>"http://clauluna.com/img/afinacion.jpg",
                                                "title"=>"Afinación"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/cambio-aceite.png",
                                                "title"=>"Cambio de Aceite"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/alinacion-balanceo.png",
                                                "title"=>"Alineación y Balanceo"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/limpieza-ajuste-frenos.png",
                                                "title"=>"Limpieza y Ajuste de Frenos"
                                            ),
                                        )
                                    ),
                                    array(
                                        "type" => "Correctivos",
                                        "child" => array(
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/amortiguadores-llantas.png",
                                                "title"=>"Amortiguadores y Llantas"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/cambio-frenos.png",
                                                "title"=>"Cambio de Frenos"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/reparacion-motores.png",
                                                "title"=>"Reparación de Motores"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/hojalateria.png",
                                                "title"=>"Hojalatería y Pintura"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/clutch.png",
                                                "title"=>"Clutch"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/transmisiones-hidraulicas.png",
                                                "title"=>"Transmisiones Hidraúlicas y Stándar"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/direcciones.png",
                                                "title"=>"Direcciones"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/distribucion.png",
                                                 "title"=>"Distribución"
                                            )
                                        )
                                    ),
                                    array(
                                        "type" => "Otros Servicios",
                                        "child" => array(
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/sistema-enfriamiento.png",
                                                "title"=>"Sistema de Enfriamiento"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/acumuladores-sistema-electrico.png",
                                                "title"=>"Acumuladores y Sistema Eléctrico"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/suspension.png",
                                                "title"=>"Suspensión"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/diagnostico.png",
                                                "title"=>"Diagnóstico con Scáner"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/verificacion.png",
                                                "title"=>"Verificación"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/sistema-escape.png",
                                                "title"=>"Sistema de Escape"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/pulido-encerado.png",
                                                "title"=>"Pulido y Encerado"
                                            ),
                                            array(
                                                "img"=>"http://clauluna.com/img/servicios/servicio-valet.png",
                                                "title"=>"Servicio de Valet"
                                            )
                                        )
                                    )
                                )
                            ),
                            array(
                                "id"=> 3,
                                "title"=> "Directorio Autocenter",
                                "content" => array(
                                    array(
                                        "zone" => "DF y Área Metropolitana",
                                        "child" => array(
                                            array(
                                                "name"=>"PLAZA INSURGENTES",
                                                "adress"=>"San Luis Potosi #214 Col. Roma, C.P. 06700 Deleg. Cuauhtemoc",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "(55)52-30-39-60"
                                                    ),
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(55)52-30-39-00"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028-1090"
                                                    )
                                                ),
                                                "mail"=>"ca0421@sears.com.mx"
                                            ),
                                            array(
                                                "name"=>"PLAZA LINDAVISTA",
                                                "adress"=>"Av. Montevideo #481 Esq. con Piura Col. Lindavista, C.P. 07300 Deleg. Gustavo A. Madero",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "51-19-91-47, 51-19-94-65"
                                                    )
                                                ),
                                                "mail"=>"ca0426@sears.com.mx"
                                            ),
                                            array(
                                                "name"=>"PLAZA UNIVERSIDAD",
                                                "adress"=>"Av. Universidad #1000 Col. Gral. Anaya, C.P. 03540 Deleg. Benito Juárez",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "(55)56-04-73-78"
                                                    ),
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(55)54-22-80-00"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028-1090"
                                                    )
                                                ),
                                                "mail"=>"ca0426@sears.com.mx"
                                            ),
                                            array(
                                                "name"=>"NEZAHUALCOYOTL",
                                                "adress"=>"Centro Comercial Jardín Bicentenario Av. Bordo de Xochiaca",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "(55)15-58-40-04"
                                                    ),
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(55)26-19-71-01"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028-2028"
                                                    )
                                                ),
                                                "mail"=>"ca0426@sears.com.mx"
                                            ),
                                            array(
                                                "name"=>"PLAZA SATÉLITE",
                                                "adress"=>"Av. Dr. Gustavo Baz #210 Fracc. Colón Echegaray, C.P. 53300 Naucalpan de Juárez Estado de México",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "53-63-89-91"
                                                    )
                                                ),
                                                "mail"=>"ca0426@sears.com.mx"
                                            ),
                                            array(
                                                "name"=>"CUAUTITLAN",
                                                "adress"=>"Centro comercial Luna Parc Av. 1º de Mayo S/N Mza. C-34 Cuautitlán Izcalli, Edo. de Méx.",
                                                "phones"=> array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(55)58-64-38-90"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1020-1098"
                                                    )
                                                ),
                                                "mail"=>"ca0426@sears.com.mx"
                                            ),
                                        )
                                    ),
                                    array(
                                        "zone"=>"Interior de la República",
                                        "child" => array(
                                            array(
                                                "name"=>"ANAHUAC",
                                                "adress"=>"Manuel Barragan No. 325 Nte. y Fray Bartolomé de las Casas Col. Residencial Anahuac, C.P. 66450 Municipio de San Nicolás de los Garza, N.L.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(81) 8318-04-05"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"PUEBLA",
                                                "adress"=>"Av. 3 Poniente #138 Centro Histórico, C.P. 72000 Puebla, Pue.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "(222) 232-5455, 242-1811"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"LEÓN PLAZA",
                                                "adress"=>"Centro Comercial Plaza Mayor (Acceso por avenida Cerro Gordo) Av. de las Torres #2002 Col. La Presa C.P. 37160 León, Guanajuato",
                                                "phones" => array(
                                                    array(
                                                        "label" =>"Tel. Directo",
                                                        "number" =>"(477) 781-1762, 781-1765"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"COATZACOALCOS",
                                                "adress"=>"Centro Comercial Forum Coatzacoalcos Antigua Carretera a Minatitlán Km. 8 Col. El Tesoro, C.P. 96536 Coatzacoalcos, Veracruz",
                                                "phones" => array(
                                                    array(
                                                        "label" =>"Tel. Directo",
                                                        "number" =>"(477) 781-1762, 781-1765"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"MONTERREY",
                                                "adress"=>"Padre Mier #143 Pte. Col. Centro, C.P. 64000 Monterrey, N",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(81) 8340-3081, 8318-0100"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1098"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"CUERNAVACA",
                                                "adress"=>"Av. Alta Tensión Esq. Río Balsas Col. Lomas de la Selva, C.P. 62270 Cuernavaca, Mor.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(777) 362-0208"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1095"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"CELAYA",
                                                "adress"=>"Centro Comercial Galerías Tecnológico Av. Torres Landa S/N Col. Del Parque, C.P. 38010 Celaya Guanajuato.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(461) 598-5920"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1095"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"GUADALAJARA CENTRO",
                                                "adress"=>"Av. 16 de Septiembre #650 Col. Centro C.P. 44100 Guadalajara, Jalisco.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Directo",
                                                        "number"=> "(33) 3614-9963"
                                                    ),
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(33) 3669-0200"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1095"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"GOMEZ PALACIO",
                                                "adress"=>"Plaza Imagen Blvd. M. Alemán y Campeche S/N Col. Las Rosas C.P. 035000 Gomez Palacio, Durango",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(871) 748-0404"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1095"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                            array(
                                                "name"=>"PACHUCA",
                                                "adress"=>"Centro Comercial Plaza Fiesta Perisur Blvd. Felipe Ángeles S/N Col. Venta Prieta, C.P. 42080 Pachuca, Hidalgo.",
                                                "phones" => array(
                                                    array(
                                                        "label"=> "Tel. Conm",
                                                        "number"=> "(771) 717-0265"
                                                    ),
                                                    array(
                                                        "label"=> "Ext.",
                                                        "number"=> "1028, 1095"
                                                    )
                                                ),
                                                "mail"=>"ca0431@sears.com.mx"            
                                            ),
                                        )
                                    )

                                )
                            )
                        )
                    )

                )
            ),
	        array(
	            "id" => 4497,
				"name" => "Hogar",
				"action" => "category2Level"
	        ),
	        array(
	            "id" => 4511,
				"name" => "Línea blanca y Electrodomésticos",
				"action" => "category2Level"
	        )
		)
		);
	    $this->JSOND(true,"TODO OK",$data,200);
}
?>